const Student = require("../models/student-model");

const createStudent = async (req, res) => {
  const { email, mobile, id } = req?.body;
  try {
    const studentExist = await Student.findOne({
      $or: [{ email: email }, { mobile: mobile }, { id: id }],
    });
    if (studentExist) {
      return res.status(409).json({
        success: false,
        message: "Data already exists! Cannot Add",
      });
    } else {
      const body = req?.body;
      const student = new Student(body);
      student.save().then(() => {
        return res.status(200).json({
          success: true,
          message: "Student Registered Successfully!!",
        });
      });
    }
  } catch (err) {
    return res.status(404).json({
      err,
      success: false,
      message: "Student Not Registered!",
    });
  }
};

const getAllStudents = (req, res) => {
  try {
    Student.find({}, (err, students) => {
      if (err) {
        return res.status(400).json({
          err,
          success: false,
          message: 'An Unexpected Error! Please Try Again'
        });
      }
      if (!students?.length) {
        return res
          .status(200)
          .json({ success: false, message: `Students List is Empty` });
      }
      return res.status(200).json({ success: true, data: students });
    });
  } catch (err) {
    return res.status(404).json({
      err,
      success: false,
      message: "An Unexpected Error! Can't Get Students",
    });
  }
};

const editStudent = async (req, res) => {
  const { name, email, mobile, id } = req?.body;
  try {
    Student.updateOne(
      { _id: req?.params?.id },
      {
        $set: {
          name: name,
          email: email,
          mobile: mobile,
        },
      },
      (err, student) => {
        if (err) {
          return res.status(400).json({
            err,
            success: false,
            message: "An Unexpected Error! Please Try Again",
          });
        }
        if (student?.modifiedCount !== 1) {
          return res.status(404).json({
            success: false,
            message: `Students Not Updated. Please Try Again..!!`,
          });
        }
        return res
          .status(200)
          .json({ success: true, message: `Student Updated Successfully` });
      }
    );
  } catch (err) {
    return res.status(404).json({
      err,
      success: false,
      message: "An Unexpected Error! Please Try Again",
    });
  }
};

const deleteStudent = (req, res) => {
  try {
    Student.findOneAndDelete({ _id: req?.params?.id }, (err, students) => {
      if (err) {
        return res.status(400).json({
          err,
          success: false,
          message:'An Unexpected Error! Please Try Again'
        });
      }
      if (!students) {
        return res
          .status(404)
          .json({ success: false, message: `Student not found` });
      }
      return res
        .status(200)
        .json({ success: true, message: `Student deleted successfully` });
    });
  } catch (err) {
    return res.status(404).json({
      err,
      success: false,
      message: "An Unexpected Error! Please Try Again",
    });
  }
};

module.exports = {
  createStudent,
  getAllStudents,
  editStudent,
  deleteStudent,
};
