# Student Registration Server

## Demo Link 
* Live-Server: https://student-registration.onrender.com/ 
* Live-Client: https://student-registration-app.onrender.com/ 

## Introduction
Student Registration is an app that is used to register students to an organization. It has a basic listing screen where list of students are displayed in a table, a basic form to get / edit the students. We can also delete a student. This web application is build using MERN Stack. This repo contains the backend code. I've used NodeJS, Express for backend and MongoDB as the database.

## Screenshots
Listing: https://screenrec.com/share/Dv4smOr8py   
Add/Edit: https://screenrec.com/share/ZAWQ9k6YKx  
Delete: https://screenrec.com/share/cuL2OHIQN8 

## Tech Stacks
`NodeJS` `Express` `MongoDB` 

## Approach
I have used Express as a middleware and routing framework that has access to the request and the response objects. Changes to the request and response is done using Express. I have used NodeJS to construct the backend server. The data is stored in and fetched from MongoDB database. There are four files :-
* db - MongoDB connection is established
* models - Holds the Schema for MongoDB Student object
* controller - MongoDB code for CRUD operations
* routes - Contains the express middleware routes for all API calls.

## Setup
* Clone the git repository
* Open command prompt and go to the cloned directory
* Run the below command to install all the dependancies 
```sh
npm install
```
* This will create node_modules and package.json file.
* To start the app run the below command
```sh
npm start
```

## Acknowledgements
* [MERN First Project](https://medium.com/swlh/how-to-create-your-first-mern-mongodb-express-js-react-js-and-node-js-stack-7e8b20463e66)
* [Express](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/routes) 
* [Deploy Code to Render.com](https://javascript.plainenglish.io/how-to-deploy-a-react-application-to-render-611ef3aca84a#:~:text=We%20can%20do%20that%20by,the%20Manual%20Deploy%20dropdown%20menu.&text=Finally%2C%20we%20can%20visit%20the,can%20deploy%20your%20React%20Applications.)
* [Render](https://render.com/docs)

## Contact
* meeravignesh2011@gmail.com
* 9790391085
