const StudentControl = require("../controllers/student-controller");
const express = require("express");
const router = express.Router();

router.post("/student", StudentControl.createStudent);
router.get("/students", StudentControl.getAllStudents);
router.delete("/student/:id", StudentControl.deleteStudent);
router.put("/student/:id", StudentControl.editStudent);

module.exports = router;
