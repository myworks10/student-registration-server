const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Student = new Schema(
  {
    name: { type: String, required: true },
    email: { type: String, required: true },
    mobile: { type: String, required: true },
    id: { type: String, required: true },
  },
  { timestamps: true }
);

module.exports = mongoose.model("students", Student);
