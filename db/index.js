const mongoose = require("mongoose");
const db = mongoose.connection;

const connect = () => {
  const uri =
    "mongodb+srv://vignesh:vignesh1309@cluster0.0zdg5he.mongodb.net/?retryWrites=true&w=majority";
  mongoose
    .connect(uri, { useNewUrlParser: true })
    .catch((err) => console.log(`Couldn't connect ${JSON.stringify(err)}`));
};

connect();

module.exports = db;
